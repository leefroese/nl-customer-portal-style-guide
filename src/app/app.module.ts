import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Provider } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { IconSpriteModule } from 'ng-svg-icon-sprite';
import { HighlightModule } from 'ngx-highlightjs';
import {NgxPageScrollModule} from 'ngx-page-scroll';
import {PageScrollConfig} from 'ngx-page-scroll';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';

import { NavComponent } from './nav/nav.component';
import { HeaderComponent } from './header/header.component';
import { GettingStartedComponent } from './getting-started/getting-started.component';
import { ColourComponent } from './colour/colour.component';
import { GridComponent } from './grid/grid.component';
import { IconsComponent } from './icons/icons.component';
import { TypographyComponent } from './typography/typography.component';
import { TablesComponent } from './tables/tables.component';
import { AccordionComponent } from './accordion/accordion.component';
import { AlertComponent } from './alert/alert.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { CarouselComponent } from './carousel/carousel.component';
import { CollapseComponent } from './collapse/collapse.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ModalComponent } from './modal/modal.component';
import { PaginationComponent } from './pagination/pagination.component';
import { PopoverComponent } from './popover/popover.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import { RatingComponent } from './rating/rating.component';
import { TabsetComponent } from './tabset/tabset.component';
import { TimepickerComponent } from './timepicker/timepicker.component';
import { TooltipComponent } from './tooltip/tooltip.component';
import { TypeaheadComponent } from './typeahead/typeahead.component';
import { NgbdModalContent } from './modal/modal.component';
import { NgbdModalConfirmAutofocus } from './modal/modal.component';
import { LeaseDetailsComponent } from './lease-details/lease-details.component';
import { BorderComponent } from './border/border.component';
import { DisplayComponent } from './display/display.component';
import { FlexboxComponent } from './flexbox/flexbox.component';
import { FloatComponent } from './float/float.component';
import { OtherComponent } from './other/other.component';
import { PositionComponent } from './position/position.component';
import { SizingComponent } from './sizing/sizing.component';
import { SpacingComponent } from './spacing/spacing.component';
import { TextComponent } from './text/text.component';
import { FormsComponent } from './forms/forms.component';
import { SelectComponent } from './select/select.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HeaderComponent,
    GettingStartedComponent,
    ColourComponent,
    GridComponent,
    IconsComponent,
    TypographyComponent,
    TablesComponent,
    AccordionComponent,
    AlertComponent,
    ButtonsComponent,
    CarouselComponent,
    CollapseComponent,
    DatepickerComponent,
    DropdownComponent,
    ModalComponent,
    PaginationComponent,
    PopoverComponent,
    ProgressbarComponent,
    RatingComponent,
    TabsetComponent,
    TimepickerComponent,
    TooltipComponent,
    TypeaheadComponent,
    NgbdModalContent,
    NgbdModalConfirmAutofocus,
    LeaseDetailsComponent,
    BorderComponent,
    DisplayComponent,
    FlexboxComponent,
    FloatComponent,
    OtherComponent,
    PositionComponent,
    SizingComponent,
    SpacingComponent,
    TextComponent,
    FormsComponent,
    SelectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    IconSpriteModule,
    HighlightModule.forRoot({ theme: 'atom-one-dark' }),
    NgxPageScrollModule,
    NgBootstrapFormValidationModule.forRoot(),
    NgBootstrapFormValidationModule
  ],
  entryComponents: [
    NgbdModalContent,
    NgbdModalConfirmAutofocus
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    PageScrollConfig.defaultDuration = 500;
    PageScrollConfig.defaultScrollOffset = 100;
  }


}
