//import { Icon } from '../app';

export const ICONS = [
    {name: 'accounts', width: '24px', height: '24px'},
    {name: 'alert', width: '20px', height: '24px'},
    {name: 'apply', width: '24px', height: '24px'},
    {name: 'avatar', width: '22px', height: '24px'},
    {name: 'back', width: '23px', height: '23px'},
    {name: 'bill-history', width: '24px', height: '24px'},
    {name: 'calendar', width: '24px', height: '24px'},
    {name: 'camera', width: '20px', height: '16px'},
    {name: 'caret-down', width: '7px', height: '5px'},
    {name: 'caret-up', width: '7px', height: '5px'},
    {name: 'caret-left', width: '5px', height: '7px'},
    {name: 'caret-right', width: '5px', height: '7px'},
    {name: 'check-badge', width: '24px', height: '24px'},
    {name: 'check', width: '24px', height: '24px'},
    {name: 'cheque', width: '16px', height: '16px'},
    {name: 'close', width: '24px', height: '24px'},
    {name: 'cog', width: '24px', height: '24px'},
    {name: 'delete', width: '23px', height: '23px'},
    {name: 'edit', width: '24px', height: '24px'},
    {name: 'error', width: '24px', height: '24px'},
    {name: 'filter', width: '24px', height: '24px'},
    {name: 'help', width: '24px', height: '24px'},
    {name: 'hide', width: '24px', height: '24px'},
    {name: 'information', width: '24px', height: '24px'},
    {name: 'loan', width: '24px', height: '24px'},
    {name: 'location', width: '18px', height: '24px'},
    {name: 'logo', width: '90px', height: '32px'},
    {name: 'logout', width: '24px', height: '24px'},
    {name: 'mail', width: '24px', height: '14px'},
    {name: 'menu', width: '21px', height: '14px'},
    {name: 'person-add', width: '24px', height: '24px'},
    {name: 'person-remove', width: '24px', height: '24px'},
    {name: 'phone', width: '25px', height: '25px'},
    {name: 'print', width: '24px', height: '24px'},
    {name: 'question', width: '25px', height: '25px'},
    {name: 'search', width: '22px', height: '22px'},
    {name: 'settings', width: '24px', height: '24px'},
    {name: 'share', width: '18px', height: '24px'},
    {name: 'show', width: '24px', height: '24px'},
    {name: 'facebook', width: '24px', height: '24px'},
    {name: 'googleplus', width: '24px', height: '24px'},
    {name: 'instagram', width: '24px', height: '24px'},
    {name: 'linkedin', width: '24px', height: '24px'},
    {name: 'twitter', width: '24px', height: '24px'},
    {name: 'youtube', width: '24px', height: '24px'},
];