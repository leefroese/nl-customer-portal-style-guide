import { Component, OnInit } from '@angular/core';
import {NgbPanelChangeEvent} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {

  // example 1
  basic_panel_1 = false;
  basic_panel_2 = true;


  // example 2
  public updateToggle($event: NgbPanelChangeEvent, acc) {

    var
      allPanels = acc.panels._results,
      panel = document.querySelector("#"+$event.panelId+"-header .toggle"),
      toggleText = panel.children[0];

    // update closed panels
    allPanels.forEach(function(pnl){
      if( pnl.id != $event.panelId ) {
        var item = document.querySelector("#"+pnl.id+"-header .toggle");
        item.children[0].textContent = "Show";
        item.children[1].innerHTML = "<use xlink:href='assets/sprites/sprite.svg#show' />";
      }
    });

    // update clicked panel
    if( toggleText.textContent == "Hide" ) {
      toggleText.textContent = "Show";
      panel.children[1].innerHTML = "<use xlink:href='assets/sprites/sprite.svg#show' />";
    } else {
      toggleText.textContent = "Hide";
      panel.children[1].innerHTML = "<use xlink:href='assets/sprites/sprite.svg#hide' />";
    }

  }

  // example 3
  public preventChange($event: NgbPanelChangeEvent) {

    if ($event.panelId === 'preventchange-2') {
      $event.preventDefault();
    }

    if ($event.panelId === 'preventchange-3' && $event.nextState === false) {
      $event.preventDefault();
    }
  }

  ngOnInit() {}

}