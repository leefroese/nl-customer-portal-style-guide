import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {

  selectedCarId = 4;
  selectedPeople1 = ['Anna Perkins', 'John Oliver'];
  selectedAccount = 'Ashley Home Furniture';

  cars = [
    { id: 1, name: 'Volvo' },
    { id: 2, name: 'Saab', disabled: true },
    { id: 3, name: 'Opel' },
    { id: 4, name: 'Audi' },
  ]

  people = [
    { id: 1, name: 'Peter Walters' },
    { id: 2, name: 'Anna Perkins' },
    { id: 3, name: 'Frances Mills' },
    { id: 4, name: 'Michael Reed' },
    { id: 5, name: 'Margaret Mendez' },
    { id: 6, name: 'John Oliver' },
    { id: 7, name: 'Ashley Howard' },
    { id: 8, name: 'Virginia Wilson' },
    { id: 9, name: 'Harold Little' },
  ]

  accounts = [
    { id: 1, name: 'Ryan Martinez Holding', altname: 'Super Investments' },
    { id: 2, name: 'John\'s Construction Inc', altname: 'Tractors Anonymous' },
    { id: 3, name: 'Ashley Home Furniture', altname: 'Dufresne Group' },
    { id: 4, name: '12345 Numbered Company', altname: '' },
    { id: 5, name: 'Zed Co.', altname: '' },
  ]

  constructor() { }

  ngOnInit() {
  }

}
