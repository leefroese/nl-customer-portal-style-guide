<div class="toc">
  <ol>
    <li><a pageScroll href="#item-1">Basic Carousel</a></li>
    <li><a pageScroll href="#item-2">Options</a></li>
  </ol>
</div>

<div class="content-wrap">

  <header class="mb-5">
    <h1 class="h2">Carousel</h1>
    <p>A ng-boostrap powered component.</p>
  </header>

  <div class="border border-light p-4" id="item-1">
    <h4>Basic Carousel</h4>
    <div class="example">
      <ngb-carousel *ngIf="images">
        <ng-template ngbSlide *ngFor="let image of images">
          <img class="d-block w-100" [src]="image" alt="Random slide">
        </ng-template>
      </ngb-carousel>
    </div>
    <ngb-tabset>
      <ngb-tab title="carousel.html">
        <ng-template ngbTabContent>
<pre><code highlight ngNonBindable>
&lt;ngb-carousel *ngIf="images">
  &lt;ng-template ngbSlide *ngFor="let image of images">
    &lt;img class="d-block w-100" [src]="image" alt="Random slide">
  &lt;/ng-template>
&lt;/ngb-carousel>
</code></pre>
        </ng-template>
      </ngb-tab>
      <ngb-tab title="carousel.ts">
        <ng-template ngbTabContent>
<pre><code highlight ngNonBindable>
import &#123; HttpClient &#125; from '@angular/common/http';
import &#123; Component &#125; from '@angular/core';
import &#123; NgbCarouselConfig &#125; from '@ng-bootstrap/ng-bootstrap';

@Component(&#123;
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  providers: [NgbCarouselConfig]
&#125;)

export class CarouselComponent &#123;

  showNavigationArrows = false;
  showNavigationIndicators = false;
  images = [1, 2, 3].map(() => &#96;https://picsum.photos/1440/600?random&t=$&#123;Math.random()&#125;&#96;);

&#125;
</code></pre>
        </ng-template>
      </ngb-tab>
    </ngb-tabset>
  </div>


  <div class="border border-light p-4 mt-5" id="item-2">
    <h4>Options</h4>
    <h5 class="mt-4">Inputs</h5>
    <table class="table table-sm"><tbody><!----><tr><td class="label-cell"><code>activeId</code><br><!----> <!----></td><td class="content-cell"><!----><p class="description">The active slide id.</p><div class="meta"><div><i>Type: </i><code>string</code></div><!----></div></td></tr><tr><td class="label-cell"><code>interval</code><br><!----> <!----></td><td class="content-cell"><!----><p class="description">Amount of time in milliseconds before next slide is shown.</p><div class="meta"><div><i>Type: </i><code>number</code></div><!----><div><span><i>Default value: </i><code>5000</code></span> <!----><span>— initialized from NgbCarouselConfig service</span></div></div></td></tr><tr><td class="label-cell"><code>keyboard</code><br><!----> <!----></td><td class="content-cell"><!----><p class="description">A flag for allowing navigation via keyboard</p><div class="meta"><div><i>Type: </i><code>boolean</code></div><!----><div><span><i>Default value: </i><code>true</code></span> <!----><span>— initialized from NgbCarouselConfig service</span></div></div></td></tr><tr><td class="label-cell"><code>pauseOnHover</code><br><!----><span class="badge badge-info">since 2.2.0</span> <!----></td><td class="content-cell"><!----><p class="description">A flag to enable slide cycling pause/resume on mouseover.</p><div class="meta"><div><i>Type: </i><code>boolean</code></div><!----><div><span><i>Default value: </i><code>true</code></span> <!----><span>— initialized from NgbCarouselConfig service</span></div></div></td></tr><tr><td class="label-cell"><code>showNavigationArrows</code><br><!----><span class="badge badge-info">since 2.2.0</span> <!----></td><td class="content-cell"><!----><p class="description">A flag to show / hide navigation arrows.</p><div class="meta"><div><i>Type: </i><code>boolean</code></div><!----><div><span><i>Default value: </i><code>true</code></span> <!----><span>— initialized from NgbCarouselConfig service</span></div></div></td></tr><tr><td class="label-cell"><code>showNavigationIndicators</code><br><!----><span class="badge badge-info">since 2.2.0</span> <!----></td><td class="content-cell"><!----><p class="description">A flag to show / hide navigation indicators.</p><div class="meta"><div><i>Type: </i><code>boolean</code></div><!----><div><span><i>Default value: </i><code>true</code></span> <!----><span>— initialized from NgbCarouselConfig service</span></div></div></td></tr><tr><td class="label-cell"><code>wrap</code><br><!----> <!----></td><td class="content-cell"><!----><p class="description">Whether can wrap from the last to the first slide.</p><div class="meta"><div><i>Type: </i><code>boolean</code></div><!----><div><span><i>Default value: </i><code>true</code></span> <!----><span>— initialized from NgbCarouselConfig service</span></div></div></td></tr></tbody></table>

    <h5 class="mt-4">Methods</h5><table class="table table-sm"><tbody><!----><tr><td class="label-cell"><code>select</code><br><!----> <!----></td><td class="content-cell"><p class="signature"><code>select(slideId: string)</code> <small class="text-muted" title="Return type">=&gt; void</small></p><!----><p class="description">Navigate to a slide with the specified identifier.</p></td></tr><tr><td class="label-cell"><code>prev</code><br><!----> <!----></td><td class="content-cell"><p class="signature"><code>prev()</code> <small class="text-muted" title="Return type">=&gt; void</small></p><!----><p class="description">Navigate to the next slide.</p></td></tr><tr><td class="label-cell"><code>next</code><br><!----> <!----></td><td class="content-cell"><p class="signature"><code>next()</code> <small class="text-muted" title="Return type">=&gt; void</small></p><!----><p class="description">Navigate to the next slide.</p></td></tr><tr><td class="label-cell"><code>pause</code><br><!----> <!----></td><td class="content-cell"><p class="signature"><code>pause()</code> <small class="text-muted" title="Return type">=&gt; void</small></p><!----><p class="description">Stops the carousel from cycling through items.</p></td></tr><tr><td class="label-cell"><code>cycle</code><br><!----> <!----></td><td class="content-cell"><p class="signature"><code>cycle()</code> <small class="text-muted" title="Return type">=&gt; void</small></p><!----><p class="description">Restarts cycling through the carousel slides from left to right.</p></td></tr></tbody></table>

  </div>


</div>