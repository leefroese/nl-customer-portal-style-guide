<div class="toc">
  <ol>
    <li><a pageScroll href="#item-1">How it Works</a></li>
    <li><a pageScroll href="#item-2">Notation</a></li>
    <li><a pageScroll href="#item-3">Examples</a></li>
    <li><a pageScroll href="#item-4">Hiding Elements</a></li>
  </ol>
</div>

<div class="content-wrap">

  <header class="mb-5">
    <h1 class="h2">Display Property</h1>
    <p>Quickly and responsively toggle the display value of components and more with our display utilities. Includes support for some of the more common values, as well as some extras for controlling display when printing.</p>
  </header>

  <div class="border border-light p-4" id="item-1">
    <h4>How it Works</h4>
    <p>Change the value of the <a href="https://developer.mozilla.org/en-US/docs/Web/CSS/display" target="_blank"><code>display</code> property</a> with our responsive display utility classes. We purposely support only a subset of all possible values for <code>display</code>. Classes can be combined for various effects as you need.</p>
  </div>



  <div class="border border-light p-4 mt-5" id="item-2">
    <h4>Notation</h4>
    <p>Display utility classes that apply to all <a href="/grid#item-2">breakpoints</a>, from <code class="highlighter-rouge">xs</code> to <code class="highlighter-rouge">xl</code>, have no breakpoint abbreviation in them. This is because those classes are applied from <code class="highlighter-rouge">min-width: 0;</code> and up, and thus are not bound by a media query. The remaining breakpoints, however, do include a breakpoint abbreviation.</p>
    <p>As such, the classes are named using the format:</p>
    <ul>
      <li><code class="highlighter-rouge">.d-&#123;value&#125;</code> for <code class="highlighter-rouge">xs</code></li>
      <li><code class="highlighter-rouge">.d-&#123;breakpoint&#125;-&#123;value&#125;</code> for <code class="highlighter-rouge">sm</code>, <code class="highlighter-rouge">md</code>, <code class="highlighter-rouge">lg</code>, and <code class="highlighter-rouge">xl</code>.</li>
    </ul>
    <p>Where <em>value</em> is one of:</p>
    <ul>
      <li><code class="highlighter-rouge">none</code></li>
      <li><code class="highlighter-rouge">inline</code></li>
      <li><code class="highlighter-rouge">inline-block</code></li>
      <li><code class="highlighter-rouge">block</code></li>
      <li><code class="highlighter-rouge">table</code></li>
      <li><code class="highlighter-rouge">table-cell</code></li>
      <li><code class="highlighter-rouge">table-row</code></li>
      <li><code class="highlighter-rouge">flex</code></li>
      <li><code class="highlighter-rouge">inline-flex</code></li>
    </ul>
    <p>The media queries effect screen widths with the given breakpoint <em>or larger</em>. For example, <code class="highlighter-rouge">.d-lg-none</code> sets <code class="highlighter-rouge">display: none;</code> on both <code class="highlighter-rouge">lg</code> and <code class="highlighter-rouge">xl</code> screens.</p>

  </div>



  <div class="border border-light p-4 mt-5" id="item-3">
    <h4>Examples</h4>
    <div class="example">
      <div class="d-inline p-2 bg-primary text-white">d-inline</div>
      <div class="d-inline p-2 bg-dark text-white">d-inline</div>
    </div>
<pre><code highlight ngNonBindable>
&lt;div class="d-inline p-2 bg-primary text-white">d-inline&lt;/div>
&lt;div class="d-inline p-2 bg-dark text-white">d-inline&lt;/div>
</code></pre>

    <div class="example">
      <span class="d-block p-2 bg-primary text-white">d-block</span>
      <span class="d-block p-2 bg-dark text-white">d-block</span>
    </div>
<pre><code highlight ngNonBindable>
&lt;span class="d-block p-2 bg-primary text-white">d-block&lt;/span>
&lt;span class="d-block p-2 bg-dark text-white">d-block&lt;/span>
</code></pre>
  </div>




  <div class="border border-light p-4 mt-5" id="item-3">
    <h4>Hiding Elements</h4>
    <p>For faster mobile-friendly development, use responsive display classes for showing and hiding elements by device. Avoid creating entirely different versions of the same site, instead hide element responsively for each screen size.</p>
    <p>To hide elements simply use the <code class="highlighter-rouge">.d-none</code> class or one of the <code class="highlighter-rouge">.d-&#123;sm,md,lg,xl&#125;-none</code> classes for any responsive screen variation.</p>
    <p>To show an element only on a given interval of screen sizes you can combine one <code class="highlighter-rouge">.d-*-none</code> class with a <code class="highlighter-rouge">.d-*-*</code> class, for example <code class="highlighter-rouge">.d-none .d-md-block .d-xl-none</code> will hide the element for all screen sizes except on medium and large devices.</p>
    <table class="table">
      <thead>
        <tr>
          <th>Screen Size</th>
          <th>Class</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Hidden on all</td>
          <td><code class="highlighter-rouge">.d-none</code></td>
        </tr>
        <tr>
          <td>Hidden only on xs</td>
          <td><code class="highlighter-rouge">.d-none .d-sm-block</code></td>
        </tr>
        <tr>
          <td>Hidden only on sm</td>
          <td><code class="highlighter-rouge">.d-sm-none .d-md-block</code></td>
        </tr>
        <tr>
          <td>Hidden only on md</td>
          <td><code class="highlighter-rouge">.d-md-none .d-lg-block</code></td>
        </tr>
        <tr>
          <td>Hidden only on lg</td>
          <td><code class="highlighter-rouge">.d-lg-none .d-xl-block</code></td>
        </tr>
        <tr>
          <td>Hidden only on xl</td>
          <td><code class="highlighter-rouge">.d-xl-none</code></td>
        </tr>
        <tr>
          <td>Visible on all</td>
          <td><code class="highlighter-rouge">.d-block</code></td>
        </tr>
        <tr>
          <td>Visible only on xs</td>
          <td><code class="highlighter-rouge">.d-block .d-sm-none</code></td>
        </tr>
        <tr>
          <td>Visible only on sm</td>
          <td><code class="highlighter-rouge">.d-none .d-sm-block .d-md-none</code></td>
        </tr>
        <tr>
          <td>Visible only on md</td>
          <td><code class="highlighter-rouge">.d-none .d-md-block .d-lg-none</code></td>
        </tr>
        <tr>
          <td>Visible only on lg</td>
          <td><code class="highlighter-rouge">.d-none .d-lg-block .d-xl-none</code></td>
        </tr>
        <tr>
          <td>Visible only on xl</td>
          <td><code class="highlighter-rouge">.d-none .d-xl-block</code></td>
        </tr>
      </tbody>
    </table>
  </div>


</div>