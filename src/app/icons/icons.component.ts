import { Component, OnInit, ViewChild } from '@angular/core';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable, Subject, merge} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import { ICONS } from '../icon-list';


@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.scss']
})
export class IconsComponent implements OnInit {

  icons: any;
  public model: any;
  selectedItem: any;

  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();

  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    const clicksWithClosedPopup$ = this.click$.pipe(filter(() => !this.instance.isPopupOpen()));
    const inputFocus$ = this.focus$;

    return merge(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(
      map(term => (term === '' ? ICONS
        : ICONS.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );

  }

  formatter = (x: {name: string}) => x.name;


  onChange(newValue) {
    this.selectedItem = newValue;  // don't forget to update the model here
    // ... do other stuff here ...
    var icon = document.getElementById('icon-'+newValue.name);
    var allIcons = document.querySelectorAll('.icon');

    if( icon ) {
      for (var i = 0; i < allIcons.length; ++i) {
        allIcons[i].classList.add('d-none');
      }
      icon.classList.remove('d-none');
    } else {
      for (var i = 0; i < allIcons.length; ++i) {
        allIcons[i].classList.remove('d-none');
      }
    }
  }

  constructor() {

  }

  ngOnInit() {
    this.icons = ICONS;
    this.model = { name: '' };
  }

}
