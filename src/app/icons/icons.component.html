
<div class="toc">
  <ol>
    <li><a pageScroll href="#item-1">Available Icons</a></li>
    <li><a pageScroll href="#item-2">Using Icons</a></li>
    <li><a pageScroll href="#item-3">Adding Icons</a></li>
    <li><a pageScroll href="#item-4">Brand Logos</a></li>
  </ol>
</div>

<div class="content-wrap">
  <header class="mb-5">
    <h1 class="h2">Icons</h1>
    <p>Icons are SVG images that have been combined into an SVG sprite for re-use throughout the website.</p>
  </header>



  <div class="border border-light p-4" id="item-1">

      <h4 class="mb-4">Available Icons</h4>

      <input
        id="typeahead-format"
        type="text"
        class="form-control mb-4"
        placeholder="Search for Icons"
        [(ngModel)]="model"
        [ngbTypeahead]="search"
        [inputFormatter]="formatter"
        [resultFormatter]="formatter"
        (focus)="focus$.next($event.target.value)"
        (click)="click$.next($event.target.value)"
        (ngModelChange)="onChange($event)"
        #instance="ngbTypeahead"
      />

      <div class="row">

        <div class="col-4 mb-5 icon" id="icon-{{ icon.name}}" *ngFor="let icon of icons">
          <div class="d-flex flex-column w-100 h-100 border border-light p-3">
            <div>
              <svg-icon-sprite
              [src]="'assets/sprites/sprite.svg#' + icon.name"
              [width]="icon.width"
              [height]="icon.height"
              ></svg-icon-sprite>
            </div>
            <p class="d-flex justify-content-between mt-3">
              <strong>#{{ icon.name }}</strong>
            </p>
            <dl class="[ d-flex flex-wrap justify-content-between ] small">
              <dt class="w-50 font-weight-normal">Width</dt><dd class="w-50 text-right"><code>{{ icon.width }}</code></dd>
              <dt class="w-50 font-weight-normal">Height</dt><dd class="w-50 text-right"><code>{{ icon.height }}</code></dd>
            </dl>
          </div>
        </div>

      </div>

  </div>



  <div class="border border-light p-4 mt-5" id="item-2">

    <h4 class="mb-4">Using Icons</h4>
    <p>The <code>ng-svg-icon-sprite</code> package makes including SVG icons in the interface easy.</p>

<pre><code highlight>
&lt;!-- including 'close' SVG from the sprite -->
&lt;svg-icon-sprite
[src]="'assets/sprites/sprite.svg#close'"
[width]="'24px'"
height]="'24px'"
>&lt;/svg-icon-sprite>
</code></pre>

    <p>If including icons in another item bound by Angular, you can use this method to include SVGs.</p>
<pre><code highlight>
&lt;svg class="d-block text-tertiary" width="16" height="16">
  &lt;use xlink:href="assets/sprites/sprite.svg#show" />
&lt;/svg>
</code></pre>

    <hr class="my-4" />
    <h5>Options</h5>
    <ul>
      <li><code>src</code> - icon source name, the syntax is <code>path/file#icon</code> where <code>path</code> is relative to app folder, <code>file</code> is
      the name of the sprite and <code>icon</code> is the filename of the svg icon.</li>
      <li><code>width</code>&nbsp;<em>(optional)</em> - width of the svg in any length unit, i.e. <code>32px</code>, <code>50%</code>, <code>auto</code> etc., default is <code>100%</code></li>
      <li><code>height</code>&nbsp;<em>(optional)</em> - the height of the svg in any length unit, if undefined height will equal the width</li>
      <li><code>classes</code>&nbsp;<em>(optional)</em> - class name for this icon, default is <code>icon</code></li>
      <li><code>viewBox</code>&nbsp;<em>(optional)</em> - define lengths and coordinates in order to scale to fit the total space available (to be used if the viewBox of the SVG is missing)</li>
      <li><code>preserveAspectRatio</code>&nbsp;<em>(optional)</em> - manipulate the aspect ratio, only in combination with <code>viewBox</code> (see SVG standard for details)</li>
    </ul>

    <hr class="my-4" />
    <h5>Scaling and Sizing</h5>
    <p>If your SVG does not scale like expected (i.e. it is cropped or larger than desired) it might be lacking a <code>viewBox</code>. You need to set the <code>viewBox</code> property manually to match the size of the exported shape. A combination of the correct <code>viewBox</code> and width is required. Add the <code>viewBox</code> property and decrease/increase the last 2 values:</p>

<pre><code highlight>
&lt;!-- i.e. lower '0 0 48 48' to '0 0 24 24' to scale up/down -->
&lt;svg-icon-sprite
  [src]="'assets/sprites/sprite.svg#close'"
  [width]="'24px'"
  [height]="'24px'"
  [viewBox]="'0 0 48 48'"
>&lt;/svg-icon-sprite>
</code></pre>

    <hr class="my-4" />
    <h5>Colouring Icons</h5>
    <p>Icons can be coloured by using the <a routerLink="/color">text color utility classes</a> such as <code>.text-primary</code>. Add these classes to the SVG markup by using the <code>[class]=""</code> option.</p>

<pre><code highlight>
  &lt;!-- i.e. lower '0 0 48 48' to '0 0 24 24' to scale up/down -->
  &lt;svg-icon-sprite
    [src]="'assets/sprites/sprite.svg#close'"
    [width]="'24px'"
    [height]="'24px'"
    [class]="'text-secondary'"
  >&lt;/svg-icon-sprite>
  </code></pre>

  </div>


  <div class="border border-light p-4 mt-5" id="item-3">

    <h4 class="mb-4">Adding Icons</h4>
    <p>The SVG sprite can be updated by:</p>
    <ol>
      <li>Export the SVG file from your editor of choice (Sketch, Illustrator, etc)</li>
      <li>Take care in naming the SVG file. Avoid using spaces or special characters. Replace spaces with hyphens instead.</li>
      <li>Add the SVG file to the <code>/src/assets/icons/ folder</code></li>
      <li>Open up your CLI of choice and navigate to your project folder <code>$ cd /sites/customer-portal</code></li>
      <li>Execute the script with <code>$ npm run create-icon-sprite</code></li>
      <li>You should see a message that your icon sprite has been created.</li>
    </ol>
    <p>Once the SVG sprite is updated, you will likely want to update this documentation. To do so:</p>
    <ol>
      <li>Open the <code>icon-list.ts</code> in <code>/src/app/</code></li>
      <li>Update the array of icons as necessary</li>
      <li>Recompile the Angular app</li>
    </ol>

  </div>


  <div class="border border-light p-4 mt-5" id="item-4">

    <h4 class="mb-4">Brand Logos</h4>
    <p>Brand logos can be saved out as SVGs from programs such as Illustrator. Here's a quick guide on some settings:</p>
    <ul>
      <li>Size the logo in pixels and adjust the artboard to be close to the same size of the logo.</li>
      <li>Save out the SVG and check off the "Use Artboards" checkbox.</li>
      <li>Change the SVG formate to "SVG Tiny 1.1".</li>
      <li>Check the "Responsive" checkbox under the advanced options.</li>
      <li>Optimize the SVG code by using <a href="https://jakearchibald.github.io/svgomg" target="_blank">https://jakearchibald.github.io/svgomg</a>.</li>
      <li>Copy the code and place it directly in your HTML markup.</li>
    </ul>
    <p>By default, the SVG will span the width of it's parent element as we've selected it to be responsive. If you want to define a width and a height, you can easily do this by adding <code>width="" height=""</code> to the SVG markup.</p>

    <div class="example">
    <svg class="d-block mb-3" baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" width="270" height="100" viewBox="0 0 270 100">
      <path fill="#6AC8CE" d="M220.1 29.5c-1.1 0-2 .4-2.7 1.1-.7.7-1.1 1.6-1.1 2.7s.4 2 1.1 2.7c.7.7 1.6 1.1 2.7 1.1s2-.4 2.7-1.1c.7-.7 1.1-1.6 1.1-2.7s-.4-2-1.1-2.7c-.7-.7-1.6-1.1-2.7-1.1z"/>
      <path fill="#6AC8CE" d="M268.4 47.8c-.4-6.3-2-12.5-4.7-18.2-2.7-5.8-6.5-10.8-11.2-15.1-4.9-4.4-10.5-7.8-16.8-10-6.3-2.2-12.8-3.1-19.3-2.7-6.3.4-12.5 2-18.2 4.7-5.8 2.8-10.9 6.5-15.1 11.2-1.3 1.4-1.2 3.5.2 4.8 1.4 1.3 3.5 1.1 4.8-.2 3.7-4.1 8.1-7.3 13.1-9.7 5-2.4 10.3-3.8 15.7-4.1 5.7-.4 11.3.4 16.7 2.3 5.4 1.9 10.3 4.8 14.5 8.6 4.1 3.7 7.3 8.1 9.7 13.1 2.4 5 3.8 10.3 4.1 15.7.3 5.7-.4 11.3-2.3 16.7-1.9 5.4-4.8 10.3-8.6 14.5-3.7 4.1-8.1 7.3-13.1 9.7-1.7.8-2.4 2.8-1.6 4.5.6 1.2 1.7 1.9 3 1.9.5 0 1-.1 1.5-.3 5.8-2.8 10.8-6.5 15.1-11.2 4.4-4.9 7.8-10.5 10-16.8 2-6.3 2.9-12.8 2.5-19.4z"/>
      <path fill="#6AC8CE" d="M220.1 43.5c-1.9 0-3.5 1.6-3.5 3.5v21.8c0 1.9 1.6 3.5 3.5 3.5s3.5-1.6 3.5-3.5V47c0-1.9-1.6-3.5-3.5-3.5z"/>
      <path fill="#0098A5" d="M175.7 86.4v11.7h-1.6V86.4h1.6z"/>
      <path fill="#0098A5" d="M5.9 20.1L18 38.8h5.5V14.4h-4.3v18.2h-.1L7.3 14.4H1.5v24.4h4.4zM33.3 33.3h10.6l2.3 5.6h5L40.6 14.4h-3.8L26.2 38.8h4.9l2.2-5.5zm5.3-13.7l3.8 9.9h-7.6l3.8-9.9zM55.7 38.8H60V18.4h7.5v-4H48.2v4h7.5zM70.4 14.4h4.3v24.4h-4.3zM101 35.9c2.4-2.4 3.7-5.5 3.7-9.3 0-3.9-1.2-7-3.6-9.3-2.4-2.3-5.5-3.5-9.3-3.5-2.6 0-4.9.6-6.9 1.7s-3.5 2.6-4.6 4.6c-1.1 2-1.6 4.2-1.6 6.7 0 3.8 1.2 6.8 3.6 9.2 2.4 2.4 5.5 3.5 9.2 3.5 3.9 0 7-1.2 9.5-3.6zm-17.7-9.4c0-2.6.8-4.6 2.4-6.3 1.6-1.6 3.6-2.4 5.9-2.4 2.5 0 4.5.8 6.1 2.5 1.6 1.7 2.3 3.7 2.3 6.2 0 2.7-.8 4.9-2.4 6.6-1.6 1.7-3.6 2.5-6.1 2.5-2.5 0-4.5-.9-6.1-2.6-1.3-1.7-2.1-3.9-2.1-6.5zM113.1 20.1h.1l12.1 18.7h5.5V14.4h-4.4v18.2l-11.9-18.2h-5.7v24.4h4.3zM158.4 38.8l-10.5-24.4h-3.8l-10.6 24.4h4.9l2.2-5.6h10.6l2.3 5.6h4.9zM142 29.5l3.8-9.9 3.8 9.9H142zM165.5 14.4h-4.4v24.4h14.7v-3.9h-10.3zM5.9 49.1H1.6v24.5h14.7v-4H5.9zM23.8 63H35v-3.9H23.8v-6h11.8v-4H19.5v24.5h16.8v-4H23.8zM48.4 49.1L37.8 73.6h4.9l2.2-5.6h10.6l2.3 5.6h5L52.2 49.1h-3.8zm-2 15.1l3.8-9.9 3.8 9.9h-7.6zM77.9 60.9c-.9-.6-2.5-1.3-4.9-2-1.2-.4-2.2-.8-2.8-1.3-.7-.5-1-1.2-1-2 0-1 .4-1.7 1.1-2.3.7-.6 1.7-.8 2.9-.8 1.9 0 3.3.6 4.3 1.9l3.2-3.3c-1.8-1.7-4.1-2.5-7-2.5-2.7 0-4.8.7-6.5 2-1.7 1.4-2.5 3.1-2.5 5.3 0 1.3.2 2.3.7 3.2.5.9 1.1 1.6 2.1 2.2.9.6 2.4 1.2 4.4 1.9 1.6.5 2.7 1 3.4 1.6.7.6 1 1.3 1 2.2s-.4 1.7-1.2 2.4c-.8.7-1.8 1-3.1 1-1 0-1.9-.2-2.8-.7-.9-.4-1.5-1.1-2.1-1.9L63.8 71c1.9 2.2 4.6 3.3 8.2 3.3 2.6 0 4.7-.7 6.3-2.1 1.6-1.4 2.4-3.3 2.4-5.7 0-1.3-.3-2.4-.8-3.3-.5-1-1.2-1.7-2-2.3zM85.1 49.1h4.3v24.4h-4.3zM116.8 73.6V49.1h-4.3v18.2h-.1l-11.8-18.2h-5.8v24.5h4.3V54.8h.1l12.1 18.8zM134.6 63.2h5V69c-1.5.9-3.4 1.3-5.7 1.3-2.5 0-4.5-.8-6.1-2.5-1.6-1.7-2.3-3.9-2.3-6.6 0-2.5.8-4.5 2.3-6.2 1.6-1.7 3.6-2.5 6-2.5 1.1 0 2.3.2 3.4.6 1.1.4 2.1 1 2.9 1.8l3.2-3.2c-2.3-2-5.5-3.1-9.6-3.1-3.9 0-7 1.2-9.4 3.6-2.4 2.4-3.6 5.5-3.6 9.4 0 3.8 1.2 6.9 3.6 9.2 2.4 2.3 5.5 3.5 9.3 3.5 3.7 0 7.1-.8 10.1-2.4V59.3h-9.3v3.9zM1.6 86.4h1.6v11.7H1.6zM26.3 96h-.1l-6.4-9.6h-2.1v11.7h1.6v-9.6l6.5 9.6h2V86.4h-1.5zM41.1 87.9h3.8v10.2h1.5V87.9h3.8v-1.5h-9.1zM65 92.9h5.6v-1.5H65v-3.5h6v-1.5h-7.6v11.7h7.9v-1.5H65zM90.1 92.8c.8-.1 1.5-.5 2.1-1 .5-.6.8-1.3.8-2 0-1-.3-1.8-1-2.4-.6-.6-1.5-.9-2.7-.9h-4.2v11.7h1.6V93h1.8l3 5.2h2l-3.4-5.4zm-3.4-1.2v-3.8h2.1c1.6 0 2.4.6 2.4 1.9 0 .6-.2 1.1-.6 1.4s-1 .5-1.8.5h-2.1zM110.4 86.4l-5.2 11.7h1.8l1.2-2.9h5.6l1.2 2.9h1.8l-5-11.7h-1.4zm-1.6 7.4l2.2-5.4 2.2 5.4h-4.4zM135.2 96.9c-1.3 0-2.3-.4-3.1-1.3-.8-.9-1.2-1.9-1.2-3.3s.4-2.5 1.2-3.4c.8-.9 1.9-1.4 3.1-1.4 1.2 0 2.1.4 2.8 1.3l1.3-1c-.5-.6-1.1-1-1.8-1.4-.7-.3-1.5-.5-2.3-.5-1.8 0-3.3.6-4.4 1.8-1.1 1.2-1.7 2.7-1.7 4.5 0 1 .3 2 .8 3s1.2 1.7 2.2 2.2c.9.5 1.9.8 3.1.8.9 0 1.8-.2 2.6-.6.8-.4 1.4-.9 1.8-1.5l-1.3-1c-.7 1.3-1.8 1.8-3.1 1.8zM151.8 87.9h3.8v10.2h1.5V87.9h3.8v-1.5h-9.1zM194 95.9l-3.5-9.5h-1.8l4.5 11.7h1.5l4.6-11.7h-1.7zM213.8 96.6v-3.7h5.6v-1.5h-5.6v-3.5h6v-1.5h-7.5v11.7h7.8v-1.5z"/>
    </svg>
    </div>

<pre><code highlight>
&lt;svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" width="270" height="100" viewBox="0 0 270 100">
  &lt;path fill="#6AC8CE" d="M220.1 29.5c-1.1 0-2 .4-2.7 1.1-.7.7-1.1 1.6-1.1 2.7s.4 2 1.1 2.7c.7.7 1.6 1.1 2.7 1.1s2-.4 2.7-1.1c.7-.7 1.1-1.6 1.1-2.7s-.4-2-1.1-2.7c-.7-.7-1.6-1.1-2.7-1.1z"/>
  &lt;path fill="#6AC8CE" d="M268.4 47.8c-.4-6.3-2-12.5-4.7-18.2-2.7-5.8-6.5-10.8-11.2-15.1-4.9-4.4-10.5-7.8-16.8-10-6.3-2.2-12.8-3.1-19.3-2.7-6.3.4-12.5 2-18.2 4.7-5.8 2.8-10.9 6.5-15.1 11.2-1.3 1.4-1.2 3.5.2 4.8 1.4 1.3 3.5 1.1 4.8-.2 3.7-4.1 8.1-7.3 13.1-9.7 5-2.4 10.3-3.8 15.7-4.1 5.7-.4 11.3.4 16.7 2.3 5.4 1.9 10.3 4.8 14.5 8.6 4.1 3.7 7.3 8.1 9.7 13.1 2.4 5 3.8 10.3 4.1 15.7.3 5.7-.4 11.3-2.3 16.7-1.9 5.4-4.8 10.3-8.6 14.5-3.7 4.1-8.1 7.3-13.1 9.7-1.7.8-2.4 2.8-1.6 4.5.6 1.2 1.7 1.9 3 1.9.5 0 1-.1 1.5-.3 5.8-2.8 10.8-6.5 15.1-11.2 4.4-4.9 7.8-10.5 10-16.8 2-6.3 2.9-12.8 2.5-19.4z"/>
  &lt;path fill="#6AC8CE" d="M220.1 43.5c-1.9 0-3.5 1.6-3.5 3.5v21.8c0 1.9 1.6 3.5 3.5 3.5s3.5-1.6 3.5-3.5V47c0-1.9-1.6-3.5-3.5-3.5z"/>
  &lt;path fill="#0098A5" d="M175.7 86.4v11.7h-1.6V86.4h1.6z"/>
  &lt;path fill="#0098A5" d="M5.9 20.1L18 38.8h5.5V14.4h-4.3v18.2h-.1L7.3 14.4H1.5v24.4h4.4zM33.3 33.3h10.6l2.3 5.6h5L40.6 14.4h-3.8L26.2 38.8h4.9l2.2-5.5zm5.3-13.7l3.8 9.9h-7.6l3.8-9.9zM55.7 38.8H60V18.4h7.5v-4H48.2v4h7.5zM70.4 14.4h4.3v24.4h-4.3zM101 35.9c2.4-2.4 3.7-5.5 3.7-9.3 0-3.9-1.2-7-3.6-9.3-2.4-2.3-5.5-3.5-9.3-3.5-2.6 0-4.9.6-6.9 1.7s-3.5 2.6-4.6 4.6c-1.1 2-1.6 4.2-1.6 6.7 0 3.8 1.2 6.8 3.6 9.2 2.4 2.4 5.5 3.5 9.2 3.5 3.9 0 7-1.2 9.5-3.6zm-17.7-9.4c0-2.6.8-4.6 2.4-6.3 1.6-1.6 3.6-2.4 5.9-2.4 2.5 0 4.5.8 6.1 2.5 1.6 1.7 2.3 3.7 2.3 6.2 0 2.7-.8 4.9-2.4 6.6-1.6 1.7-3.6 2.5-6.1 2.5-2.5 0-4.5-.9-6.1-2.6-1.3-1.7-2.1-3.9-2.1-6.5zM113.1 20.1h.1l12.1 18.7h5.5V14.4h-4.4v18.2l-11.9-18.2h-5.7v24.4h4.3zM158.4 38.8l-10.5-24.4h-3.8l-10.6 24.4h4.9l2.2-5.6h10.6l2.3 5.6h4.9zM142 29.5l3.8-9.9 3.8 9.9H142zM165.5 14.4h-4.4v24.4h14.7v-3.9h-10.3zM5.9 49.1H1.6v24.5h14.7v-4H5.9zM23.8 63H35v-3.9H23.8v-6h11.8v-4H19.5v24.5h16.8v-4H23.8zM48.4 49.1L37.8 73.6h4.9l2.2-5.6h10.6l2.3 5.6h5L52.2 49.1h-3.8zm-2 15.1l3.8-9.9 3.8 9.9h-7.6zM77.9 60.9c-.9-.6-2.5-1.3-4.9-2-1.2-.4-2.2-.8-2.8-1.3-.7-.5-1-1.2-1-2 0-1 .4-1.7 1.1-2.3.7-.6 1.7-.8 2.9-.8 1.9 0 3.3.6 4.3 1.9l3.2-3.3c-1.8-1.7-4.1-2.5-7-2.5-2.7 0-4.8.7-6.5 2-1.7 1.4-2.5 3.1-2.5 5.3 0 1.3.2 2.3.7 3.2.5.9 1.1 1.6 2.1 2.2.9.6 2.4 1.2 4.4 1.9 1.6.5 2.7 1 3.4 1.6.7.6 1 1.3 1 2.2s-.4 1.7-1.2 2.4c-.8.7-1.8 1-3.1 1-1 0-1.9-.2-2.8-.7-.9-.4-1.5-1.1-2.1-1.9L63.8 71c1.9 2.2 4.6 3.3 8.2 3.3 2.6 0 4.7-.7 6.3-2.1 1.6-1.4 2.4-3.3 2.4-5.7 0-1.3-.3-2.4-.8-3.3-.5-1-1.2-1.7-2-2.3zM85.1 49.1h4.3v24.4h-4.3zM116.8 73.6V49.1h-4.3v18.2h-.1l-11.8-18.2h-5.8v24.5h4.3V54.8h.1l12.1 18.8zM134.6 63.2h5V69c-1.5.9-3.4 1.3-5.7 1.3-2.5 0-4.5-.8-6.1-2.5-1.6-1.7-2.3-3.9-2.3-6.6 0-2.5.8-4.5 2.3-6.2 1.6-1.7 3.6-2.5 6-2.5 1.1 0 2.3.2 3.4.6 1.1.4 2.1 1 2.9 1.8l3.2-3.2c-2.3-2-5.5-3.1-9.6-3.1-3.9 0-7 1.2-9.4 3.6-2.4 2.4-3.6 5.5-3.6 9.4 0 3.8 1.2 6.9 3.6 9.2 2.4 2.3 5.5 3.5 9.3 3.5 3.7 0 7.1-.8 10.1-2.4V59.3h-9.3v3.9zM1.6 86.4h1.6v11.7H1.6zM26.3 96h-.1l-6.4-9.6h-2.1v11.7h1.6v-9.6l6.5 9.6h2V86.4h-1.5zM41.1 87.9h3.8v10.2h1.5V87.9h3.8v-1.5h-9.1zM65 92.9h5.6v-1.5H65v-3.5h6v-1.5h-7.6v11.7h7.9v-1.5H65zM90.1 92.8c.8-.1 1.5-.5 2.1-1 .5-.6.8-1.3.8-2 0-1-.3-1.8-1-2.4-.6-.6-1.5-.9-2.7-.9h-4.2v11.7h1.6V93h1.8l3 5.2h2l-3.4-5.4zm-3.4-1.2v-3.8h2.1c1.6 0 2.4.6 2.4 1.9 0 .6-.2 1.1-.6 1.4s-1 .5-1.8.5h-2.1zM110.4 86.4l-5.2 11.7h1.8l1.2-2.9h5.6l1.2 2.9h1.8l-5-11.7h-1.4zm-1.6 7.4l2.2-5.4 2.2 5.4h-4.4zM135.2 96.9c-1.3 0-2.3-.4-3.1-1.3-.8-.9-1.2-1.9-1.2-3.3s.4-2.5 1.2-3.4c.8-.9 1.9-1.4 3.1-1.4 1.2 0 2.1.4 2.8 1.3l1.3-1c-.5-.6-1.1-1-1.8-1.4-.7-.3-1.5-.5-2.3-.5-1.8 0-3.3.6-4.4 1.8-1.1 1.2-1.7 2.7-1.7 4.5 0 1 .3 2 .8 3s1.2 1.7 2.2 2.2c.9.5 1.9.8 3.1.8.9 0 1.8-.2 2.6-.6.8-.4 1.4-.9 1.8-1.5l-1.3-1c-.7 1.3-1.8 1.8-3.1 1.8zM151.8 87.9h3.8v10.2h1.5V87.9h3.8v-1.5h-9.1zM194 95.9l-3.5-9.5h-1.8l4.5 11.7h1.5l4.6-11.7h-1.7zM213.8 96.6v-3.7h5.6v-1.5h-5.6v-3.5h6v-1.5h-7.5v11.7h7.8v-1.5z"/>
&lt;/svg>
</code></pre>

  </div>

</div>
