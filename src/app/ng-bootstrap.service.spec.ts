import { TestBed, inject } from '@angular/core/testing';

import { NgBootstrapService } from './ng-bootstrap.service';

describe('NgBootstrapService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgBootstrapService]
    });
  });

  it('should be created', inject([NgBootstrapService], (service: NgBootstrapService) => {
    expect(service).toBeTruthy();
  }));
});
