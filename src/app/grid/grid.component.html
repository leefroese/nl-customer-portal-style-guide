<div class="toc">
  <ol>
    <li><a pageScroll href="#item-1">Grid Setup</a></li>
    <li><a pageScroll href="#item-2">Grid Breakpoints</a></li>
    <li><a pageScroll href="#item-3">Auto-Layout Columns</a></li>
    <li><a pageScroll href="#item-4">Responsive Classes</a></li>
    <li><a pageScroll href="#item-5">Column Alignment</a></li>
    <li><a pageScroll href="#item-6">Column Gutters</a></li>
    <li><a pageScroll href="#item-7">Column Breaks</a></li>
    <li><a pageScroll href="#item-8">Ordering Columns</a></li>
    <li><a pageScroll href="#item-9">Offsetting Columns</a></li>
    <li><a pageScroll href="#item-10">Spacing Columns</a></li>
    <li><a pageScroll href="#item-11">Nesting Columns</a></li>
  </ol>
</div>

<div class="content-wrap">

  <header class="mb-5">
    <h1 class="h2">Grid</h1>
    <p>The grid system uses Bootstrap 4's mobile-first flexbox grid. The system uses a series of containers, rows, and columns to layout and align content.</p>
  </header>

  <div class="border border-light p-4" id="item-1">

    <h4 class="mb-4">Grid Setup</h4>
    <p>Here's an example of a very basic grid setup. This example creates three equal-width columns on small, medium, large, and extra large devices using our predefined grid classes. Those columns are centered in the page with the parent <code>.container</code>.</p>
    <div class="container mb-3">
      <div class="row">
        <div class="col-4 bg-offwhite border border-medium">
          <p class="m-3 text-center">Column One</p>
        </div>
        <div class="col-4 bg-offwhite border border-medium">
          <p class="m-3 text-center">Column Two</p>
        </div>
        <div class="col-4 bg-offwhite border border-medium">
          <p class="m-3 text-center">Column Three</p>
        </div>
      </div>
    </div>

<pre><code highlight>
&lt;div class="container">
  &lt;div class="row">
    &lt;div class="col-4">Column One&lt;/div>
    &lt;div class="col-4">Column Two&lt;/div>
    &lt;div class="col-4">Column Three&lt;/div>
  &lt;/div>
&lt;/div>
</code></pre>

    <p>Breaking it down further, here’s how the grid works:</p>
    <ul>
      <li>Containers provide a means to center and horizontally pad your site’s contents. Use <code>.container</code> for a responsive pixel width or <code>.container-fluid</code> for <code>width: 100%</code> across all viewport and device sizes.</li>
      <li>Rows are wrappers for columns. Each column has horizontal <code>padding</code> (called a gutter) for controlling the space between them. This <code>padding</code> is then counteracted on the rows with negative margins. This way, all the content in your columns is visually aligned down the left side.</li>
      <li>In a grid layout, content must be placed within columns and only columns may be immediate children of rows.</li>
      <li>Thanks to flexbox, grid columns without a specified <code>width</code> will automatically layout as equal width columns. For example, four instances of <code>.col-sm</code> will each automatically be 25% wide from the small breakpoint and up. See the <a href="#auto-layout-columns">auto-layout columns</a> section for more examples.</li>
      <li>Column classes indicate the number of columns you’d like to use out of the possible 12 per row. So, if you want three equal-width columns across, you can use <code>.col-4</code>.</li>
      <li>Column <code>width</code>s are set in percentages, so they’re always fluid and sized relative to their parent element.</li>
      <li>Columns have horizontal <code>padding</code> to create the gutters between individual columns, however, you can remove the <code>margin</code> from rows and <code>padding</code> from columns with <code>.no-gutters</code> on the <code>.row</code>.</li>
      <li>To make the grid responsive, there are five grid breakpoints, one for each <a routerLink="/responsive-breakpoints">responsive breakpoint</a>: all breakpoints (extra small), small, medium, large, and extra large.</li>
      <li>Grid breakpoints are based on minimum width media queries, meaning <strong>they apply to that one breakpoint and all those above it</strong> (e.g., <code>.col-sm-4</code> applies to small, medium, large, and extra large devices, but not the first <code>xs</code> breakpoint).</li>
    </ul>


  </div>


  <div class="border border-light p-4 mt-5" id="item-2">

    <h4 class="mb-4">Grid Breakpoints</h4>
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th></th>
          <th class="text-center">
            Extra small<br>
            <small>&lt;576px</small>
          </th>
          <th class="text-center">
            Small<br>
            <small>≥576px</small>
          </th>
          <th class="text-center">
            Medium<br>
            <small>≥768px</small>
          </th>
          <th class="text-center">
            Large<br>
            <small>≥992px</small>
          </th>
          <th class="text-center">
            Extra large<br>
            <small>≥1200px</small>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th class="text-nowrap" scope="row">Max container width</th>
          <td>None (auto)</td>
          <td>540px</td>
          <td>720px</td>
          <td>960px</td>
          <td>1140px</td>
        </tr>
        <tr>
          <th class="text-nowrap" scope="row">Class prefix</th>
          <td><code>.col-</code></td>
          <td><code>.col-sm-</code></td>
          <td><code>.col-md-</code></td>
          <td><code>.col-lg-</code></td>
          <td><code>.col-xl-</code></td>
        </tr>
        <tr>
          <th class="text-nowrap" scope="row"># of columns</th>
          <td colspan="5">12</td>
        </tr>
        <tr>
          <th class="text-nowrap" scope="row">Gutter width</th>
          <td colspan="5">40px (20px on each side of a column)</td>
        </tr>
        <tr>
          <th class="text-nowrap" scope="row">Nestable</th>
          <td colspan="5">Yes</td>
        </tr>
        <tr>
          <th class="text-nowrap" scope="row">Column ordering</th>
          <td colspan="5">Yes</td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="border border-light p-4 mt-5" id="item-3">

      <h4 class="mb-4">Auto-Layout Columns</h4>
      <p>Utilize breakpoint-specific column classes for easy column sizing without an explicit numbered class like <code>.col-sm-6</code>.</p>

      <hr class="my-4" />
      <h5>Equal-Width Columns</h5>
      <p>For example, here are two grid layouts that apply to every device and viewport, from <code>xs</code> to <code>xl</code>. Add any number of unit-less classes for each breakpoint you need and every column will be the same width.</p>
      <div class="example">
        <div class="container">
          <div class="row">
            <div class="col bg-offwhite border border-medium">
              <p class="m-3 text-center">1 of 2</p>
            </div>
            <div class="col bg-offwhite border border-medium">
              <p class="m-3 text-center">2 of 2</p>
            </div>
          </div>
          <div class="row">
            <div class="col bg-offwhite border border-medium">
              <p class="m-3 text-center">1 of 3</p>
            </div>
            <div class="col bg-offwhite border border-medium">
              <p class="m-3 text-center">2 of 3</p>
            </div>
            <div class="col bg-offwhite border border-medium">
              <p class="m-3 text-center">3 of 3</p>
            </div>
          </div>
        </div>
      </div>

<pre><code highlight>
&lt;div class="container">
  &lt;div class="row">
    &lt;div class="col">1 of 2&lt;/div>
    &lt;div class="col">2 of 2&lt;/div>
  &lt;/div>
  &lt;div class="row">
    &lt;div class="col">1 of 3&lt;/div>
    &lt;div class="col">2 of 3&lt;/div>
    &lt;div class="col">3 of 3&lt;/div>
  &lt;/div>
&lt;/div>
</code></pre>

    <hr class="my-4" />
    <h5>Setting One Column Width</h5>
    <p>Auto-layout for flexbox grid columns also means you can set the width of one column and have the sibling columns automatically resize around it. You may use predefined grid classes (as shown below), grid mixins, or inline widths. Note that the other columns will resize no matter the width of the center column.</p>
    <div class="example">
      <div class="container">
        <div class="row">
          <div class="col bg-offwhite border border-medium">
            <p class="m-3 text-center">1 of 3</p>
          </div>
          <div class="col-6 bg-offwhite border border-medium">
            <p class="m-3 text-center">2 of 3 (wider)</p>
          </div>
          <div class="col bg-offwhite border border-medium">
            <p class="m-3 text-center">3 of 3</p>
          </div>
        </div>
        <div class="row">
          <div class="col bg-offwhite border border-medium">
            <p class="m-3 text-center">1 of 3</p>
          </div>
          <div class="col-5 bg-offwhite border border-medium">
            <p class="m-3 text-center">2 of 3 (wider)</p>
          </div>
          <div class="col bg-offwhite border border-medium">
            <p class="m-3 text-center">3 of 3</p>
          </div>
        </div>
      </div>
    </div>


<pre><code highlight>
&lt;div class="container">
  &lt;div class="row">
    &lt;div class="col">1 of 3&lt;/div>
    &lt;div class="col-6">2 of 3 (wider)&lt;/div>
    &lt;div class="col">3 of 3&lt;/div>
  &lt;/div>
  &lt;div class="row">
    &lt;div class="col">1 of 3&lt;/div>
    &lt;div class="col-5">2 of 3 (wider)&lt;/div>
    &lt;div class="col">3 of 3&lt;/div>
  &lt;/div>
&lt;/div>
</code></pre>

    <hr class="my-4" />
    <h5>Variable Width Content</h5>
    <p>Use <code>col-(breakpoint)-auto</code> classes to size columns based on the natural width of their content.</p>

    <div class="example">
      <div class="container">
        <div class="row justify-content-md-center">
          <div class="col col-lg-2">1 of 3</div>
          <div class="col-md-auto">Variable width content</div>
          <div class="col col-lg-2">3 of 3</div>
        </div>
        <div class="row">
          <div class="col">1 of 3</div>
          <div class="col-md-auto">Variable width content</div>
          <div class="col col-lg-2">3 of 3</div>
        </div>
      </div>
    </div>

<pre><code highlight>
&lt;div class="container">
  &lt;div class="row justify-content-md-center">
    &lt;div class="col col-lg-2">1 of 3&lt;/div>
    &lt;div class="col-md-auto">Variable width content&lt;/div>
    &lt;div class="col col-lg-2">3 of 3&lt;/div>
  &lt;/div>
  &lt;div class="row">
    &lt;div class="col">1 of 3&lt;/div>
    &lt;div class="col-md-auto">Variable width content&lt;/div>
    &lt;div class="col col-lg-2">3 of 3&lt;/div>
  &lt;/div>
&lt;/div>
</code></pre>

  </div>


  <div class="border border-light p-4 mt-5" id="item-4">

      <h4 class="mb-4">Responsive Classes</h4>
      <p>Bootstrap’s grid includes five tiers of predefined classes for building complex responsive layouts. Customize the size of your columns on extra small, small, medium, large, or extra large devices however you see fit.</p>

      <hr class="my-4" />
      <h5>Stacked to Horizontal</h5>
      <p>Using a single set of <code>.col-sm-*</code> classes, you can create a basic grid system that starts out stacked before becoming horizontal with at the small breakpoint (sm).</p>
      <div class="example">
        <div class="container">
          <div class="row">
            <div class="col-sm-8">col-sm-8</div>
            <div class="col-sm-4">col-sm-4</div>
          </div>
        </div>
      </div>

<pre><code highlight>
&lt;div class="row">
  &lt;div class="col-sm-8">col-sm-8&lt;/div>
  &lt;div class="col-sm-4">col-sm-4&lt;/div>
&lt;/div>
</code></pre>

    <hr class="my-4" />
    <h5>Mix and Match</h5>
    <p>Don’t want your columns to simply stack in some grid tiers? Use a combination of different classes for each tier as needed. See the example below for a better idea of how it all works.</p>

    <div class="example example-rows">
      <div class="row">
        <div class="col-12 col-md-8">.col-12 .col-md-8</div>
        <div class="col-6 col-md-4">.col-6 .col-md-4</div>
      </div>
      <div class="row">
        <div class="col-6 col-md-4">.col-6 .col-md-4</div>
        <div class="col-6 col-md-4">.col-6 .col-md-4</div>
        <div class="col-6 col-md-4">.col-6 .col-md-4</div>
      </div>
      <div class="row">
        <div class="col-6">.col-6</div>
        <div class="col-6">.col-6</div>
      </div>
    </div>

<pre><code highlight>
&lt;!-- Stack the columns on mobile by making one full-width and the other half-width -->
&lt;div class="row">
  &lt;div class="col-12 col-md-8">.col-12 .col-md-8&lt;/div>
  &lt;div class="col-6 col-md-4">.col-6 .col-md-4&lt;/div>
&lt;/div>

&lt;!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
&lt;div class="row">
  &lt;div class="col-6 col-md-4">.col-6 .col-md-4&lt;/div>
  &lt;div class="col-6 col-md-4">.col-6 .col-md-4&lt;/div>
  &lt;div class="col-6 col-md-4">.col-6 .col-md-4&lt;/div>
&lt;/div>

&lt;!-- Columns are always 50% wide, on mobile and desktop -->
&lt;div class="row">
  &lt;div class="col-6">.col-6&lt;/div>
  &lt;div class="col-6">.col-6&lt;/div>
&lt;/div>
</code></pre>

  </div>


  <div class="border border-light p-4 mt-5" id="item-5">

      <h4 class="mb-4">Column Alignment</h4>
      <p>Use flexbox alignment utilities to vertically and horizontally align columns.</p>

      <hr class="my-4"/>
      <h5>Vertical Alignment</h5>

      <div class="example example-rows example-min-rows">
        <div class="container">
          <div class="row align-items-start">
            <div class="col">
              One of three columns
            </div>
            <div class="col">
              One of three columns
            </div>
            <div class="col">
              One of three columns
            </div>
          </div>
          <div class="row align-items-center">
            <div class="col">
              One of three columns
            </div>
            <div class="col">
              One of three columns
            </div>
            <div class="col">
              One of three columns
            </div>
          </div>
          <div class="row align-items-end">
            <div class="col">
              One of three columns
            </div>
            <div class="col">
              One of three columns
            </div>
            <div class="col">
              One of three columns
            </div>
          </div>
        </div>
      </div>

<pre><code highlight>
&lt;!-- vertically align top -->
&lt;div class="row align-items-start">
  &lt;div class="col">One of three columns&lt;/div>
  &lt;div class="col">One of three columns&lt;/div>
  &lt;div class="col">One of three columns&lt;/div>
&lt;/div>
&lt;!-- vertically align center -->
&lt;div class="row align-items-center">
  &lt;div class="col">One of three columns&lt;/div>
  &lt;div class="col">One of three columns&lt;/div>
  &lt;div class="col">One of three columns&lt;/div>
&lt;/div>
&lt;!-- vertically align bottom -->
&lt;div class="row align-items-end">
  &lt;div class="col">One of three columns&lt;/div>
  &lt;div class="col">One of three columns&lt;/div>
  &lt;div class="col">One of three columns&lt;/div>
&lt;/div>
</code></pre>

    <div class="example example-rows example-min-rows">
      <div class="container">
        <div class="row">
          <div class="col align-self-start">One of three columns</div>
          <div class="col align-self-center">One of three columns</div>
          <div class="col align-self-end">One of three columns</div>
        </div>
      </div>
    </div>

<pre><code highlight>
&lt;div class="row">
  &lt;div class="col align-self-start">One of three columns&lt;/div>
  &lt;div class="col align-self-center">One of three columns&lt;/div>
  &lt;div class="col align-self-end">One of three columns&lt;/div>
&lt;/div>
</code></pre>

    <hr class="my-4"/>
    <h5>Horizontal Alignment</h5>

    <div class="example example-rows">
      <div class="container">
        <div class="row justify-content-start">
          <div class="col-4">One of two columns</div>
          <div class="col-4">One of two columns</div>
        </div>
        <div class="row justify-content-center">
          <div class="col-4">One of two columns</div>
          <div class="col-4">One of two columns</div>
        </div>
        <div class="row justify-content-end">
          <div class="col-4">One of two columns</div>
          <div class="col-4">One of two columns</div>
        </div>
        <div class="row justify-content-around">
          <div class="col-4">One of two columns</div>
          <div class="col-4">One of two columns</div>
        </div>
        <div class="row justify-content-between">
          <div class="col-4">One of two columns</div>
          <div class="col-4">One of two columns</div>
        </div>
      </div>
    </div>

<pre><code highlight>
&lt;!-- horizontally align left -->
&lt;div class="row justify-content-start">
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
&lt;/div>
&lt;!-- horizontally align center -->
&lt;div class="row justify-content-center">
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
&lt;/div>
&lt;!-- horizontally align center -->
&lt;div class="row justify-content-end">
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
&lt;/div>
&lt;!-- horizontally align items with even spacing around them -->
&lt;div class="row justify-content-around">
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
&lt;/div>
&lt;!-- horizontally align items with even spacing between them -->
&lt;div class="row justify-content-between">
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
  &lt;div class="col-4">
    One of two columns
  &lt;/div>
&lt;/div>
</code></pre>
  </div>


  <div class="border border-light p-4 mt-5" id="item-6">

      <h4 class="mb-4">Column Gutters</h4>
      <p>By default, columns have a 40px gutter between them and the <code>.row</code> element has negative margins to accomodate this.</p>
      <p>These gutters between columns can be removed by adding <code>.no-gutters</code> to the <code>.row</code> element. This removes the negative margins from <code>.row</code> and the horizontal padding from all immediate children columns.</p>

      <div class="example">
        <div class="container">
          <div class="row no-gutters">
            <div class="col-12 col-sm-6 col-md-8">.col-12 .col-sm-6 .col-md-8</div>
            <div class="col-6 col-md-4">.col-6 .col-md-4</div>
          </div>
        </div>
      </div>

<pre><code highlight>
&lt;div class="row no-gutters">
  &lt;div class="col-12 col-sm-6 col-md-8">.col-12 .col-sm-6 .col-md-8&lt;/div>
  &lt;div class="col-6 col-md-4">.col-6 .col-md-4&lt;/div>
&lt;/div>
</code></pre>

  </div>


  <div class="border border-light p-4 mt-5" id="item-7">

      <h4 class="mb-4">Column Breaks</h4>
      <p>Breaking columns to a new line in flexbox requires a small hack: add an element with <code>width: 100%</code> wherever you want to wrap your columns to a new line. Normally this is accomplished with multiple <code>.row</code>s, but not every implementation method can account for this.</p>


      <div class="example">
        <div class="container">
          <div class="row">
              <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
              <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
              <div class="w-100"></div>
              <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
              <div class="col-6 col-sm-3">.col-6 .col-sm-3</div>
            </div>
        </div>
      </div>

<pre><code highlight>
&lt;div class="row">
  &lt;div class="col-6 col-sm-3">.col-6 .col-sm-3&lt;/div>
  &lt;div class="col-6 col-sm-3">.col-6 .col-sm-3&lt;/div>

  &lt;!-- Force next columns to break to new line -->
  &lt;div class="w-100">&lt;/div>

  &lt;div class="col-6 col-sm-3">.col-6 .col-sm-3&lt;/div>
  &lt;div class="col-6 col-sm-3">.col-6 .col-sm-3&lt;/div>
&lt;/div>
</code></pre>
  </div>



  <div class="border border-light p-4 mt-5" id="item-8">

      <h4 class="mb-4">Ordering Columns</h4>
      <p>Use <code>.order-</code> classes for controlling the <strong>visual order</strong> of your content. These classes are responsive, so you can set the <code>order</code> by breakpoint (e.g., <code>.order-1.order-md-2</code>). Includes support for <code>1</code> through <code>12</code> across all five grid tiers.</p>
      <p>There are also responsive <code>.order-first</code> and <code>.order-last</code> classes that change the <code>order</code> of an element by applying <code>order: -1</code> and <code>order: 13</code> (<code>order: $columns + 1</code>), respectively. These classes can also be intermixed with the numbered <code>.order-*</code> classes as needed.</p>

      <div class="example example-rows">
        <div class="container">
          <div class="row">
            <div class="col">First, but unordered</div>
            <div class="col order-12">Second, but last</div>
            <div class="col order-1">Third, but second</div>
          </div>
          <div class="row">
            <div class="col order-lg-last">First, but last at the lg breakpoint</div>
            <div class="col">Second, but unordered</div>
            <div class="col order-lg-first">Third, but first at the lg breakpoint</div>
          </div>
        </div>
      </div>

<pre><code highlight>
&lt;div class="row">
  &lt;div class="col">First, but unordered&lt;/div>
  &lt;div class="col order-12">Second, but last&lt;/div>
  &lt;div class="col order-1">Third, but second&lt;/div>
&lt;/div>
&lt;div class="row">
  &lt;div class="col order-lg-last">First, but last at the lg breakpoint&lt;/div>
  &lt;div class="col">Second, but unordered&lt;/div>
  &lt;div class="col order-lg-first">Third, but first at the lg breakpoint&lt;/div>
&lt;/div>
</code></pre>
  </div>



  <div class="border border-light p-4 mt-5" id="item-9">

      <h4 class="mb-4">Offsetting Columns</h4>
      <p>You can offset grid columns in two ways: our responsive <code>.offset-</code> grid classes and our <a routerLink="/spacing">margin utilities</a>. Grid classes are sized to match columns while margins are more useful for quick layouts where the width of the offset is variable.</p>
      <p>Move columns to the right using <code>.offset-md-*</code> classes. These classes increase the left margin of a column by <code>*</code> columns. For example, <code>.offset-md-4</code> moves <code>.col-md-4</code> over four columns.</p>
      <div class="example example-rows">
        <div class="container">
          <div class="row">
            <div class="col-md-4">.col-md-4</div>
            <div class="col-md-4 offset-md-4">.col-md-4 .offset-md-4</div>
          </div>
          <div class="row">
            <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
            <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
          </div>
          <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-0">.col-md-6 .offset-md-3 .offset-lg-0</div>
          </div>
        </div>
      </div>

<pre><code highlight>
&lt;div class="row">
  &lt;div class="col-md-4">.col-md-4&lt;/div>
  &lt;div class="col-md-4 offset-md-4">.col-md-4 .offset-md-4&lt;/div>
&lt;/div>
&lt;div class="row">
  &lt;div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3&lt;/div>
  &lt;div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3&lt;/div>
&lt;/div>
&lt;!-- Reset offset at lg breakpoint -->
&lt;div class="row">
  &lt;div class="col-md-6 offset-md-3 offset-lg-0">.col-md-6 .offset-md-3 .offset-lg-0&lt;/div>
&lt;/div>
</code></pre>
 </div>


  <div class="border border-light p-4 mt-5" id="item-10">

    <h4 class="mb-4">Spacing Columns</h4>
    <p>With flexbox, you can use margin utilities like <code>.mr-auto</code> to force sibling columns away from one another.</p>
    <div class="example example-rows">
      <div class="container">
        <div class="row">
          <div class="col-md-4">.col-md-4</div>
          <div class="col-md-4 ml-auto">.col-md-4 .ml-auto</div>
        </div>
        <div class="row">
          <div class="col-md-3 ml-md-auto">.col-md-3 .ml-md-auto</div>
          <div class="col-md-3 ml-md-auto">.col-md-3 .ml-md-auto</div>
        </div>
        <div class="row">
          <div class="col-auto mr-auto">.col-auto .mr-auto</div>
          <div class="col-auto">.col-auto</div>
        </div>
      </div>
    </div>

<pre><code highlight>
  &lt;div class="row">
    &lt;div class="col-md-4">.col-md-4&lt;/div>
    &lt;div class="col-md-4 ml-auto">.col-md-4 .ml-auto&lt;/div>
  &lt;/div>
  &lt;div class="row">
    &lt;div class="col-md-3 ml-md-auto">.col-md-3 .ml-md-auto&lt;/div>
    &lt;div class="col-md-3 ml-md-auto">.col-md-3 .ml-md-auto&lt;/div>
  &lt;/div>
  &lt;div class="row">
    &lt;div class="col-auto mr-auto">.col-auto .mr-auto&lt;/div>
    &lt;div class="col-auto">.col-auto&lt;/div>
  &lt;/div>
</code></pre>

  </div>




  <div class="border border-light p-4 mt-5" id="item-11">

    <h4 class="mb-4">Nesting Columns</h4>
    <p>To nest your content with the default grid, add a new <code>.row</code> and set of <code>.col-sm-*</code> columns within an existing <code>.col-sm-*</code> column. Nested rows should include a set of columns that add up to 12 or fewer (it is not required that you use all 12 available columns).</p>
    <div class="example example-nested">
      <div class="row">
        <div class="col-sm-9">
          Level 1: .col-sm-9
          <div class="row">
            <div class="col-8 col-sm-6">
              Level 2: .col-8 .col-sm-6
            </div>
            <div class="col-4 col-sm-6">
              Level 2: .col-4 .col-sm-6
            </div>
          </div>
        </div>
      </div>
    </div>

<pre><code highlight>
&lt;div class="row">
  &lt;div class="col-sm-9">
    Level 1: .col-sm-9
    &lt;div class="row">
      &lt;div class="col-8 col-sm-6">
        Level 2: .col-8 .col-sm-6
      &lt;/div>
      &lt;div class="col-4 col-sm-6">
        Level 2: .col-4 .col-sm-6
      &lt;/div>
    &lt;/div>
  &lt;/div>
&lt;/div>
</code></pre>

  </div>

</div>