import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GettingStartedComponent } from './getting-started/getting-started.component';

import { ColourComponent } from './colour/colour.component';
import { FormsComponent } from './forms/forms.component';
import { GridComponent } from './grid/grid.component';
import { IconsComponent } from './icons/icons.component';
import { TablesComponent } from './tables/tables.component';
import { TypographyComponent } from './typography/typography.component';
import { AccordionComponent } from './accordion/accordion.component';
import { AlertComponent } from './alert/alert.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { CarouselComponent } from './carousel/carousel.component';
import { CollapseComponent } from './collapse/collapse.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ModalComponent } from './modal/modal.component';
import { PaginationComponent } from './pagination/pagination.component';
import { PopoverComponent } from './popover/popover.component';
import { ProgressbarComponent } from './progressbar/progressbar.component';
import { RatingComponent } from './rating/rating.component';
import { TabsetComponent } from './tabset/tabset.component';
import { TimepickerComponent } from './timepicker/timepicker.component';
import { TooltipComponent } from './tooltip/tooltip.component';
import { TypeaheadComponent } from './typeahead/typeahead.component';
import { LeaseDetailsComponent } from './lease-details/lease-details.component';
import { BorderComponent } from './border/border.component';
import { DisplayComponent } from './display/display.component';
import { FlexboxComponent } from './flexbox/flexbox.component';
import { FloatComponent } from './float/float.component';
import { OtherComponent } from './other/other.component';
import { PositionComponent } from './position/position.component';
import { SizingComponent } from './sizing/sizing.component';
import { SpacingComponent } from './spacing/spacing.component';
import { TextComponent } from './text/text.component';
import { SelectComponent } from './select/select.component';

const routes: Routes = [
  {
    path: '',
    component: GettingStartedComponent
  },
  {
    path: 'colour',
    component: ColourComponent
  },
  {
    path: 'forms',
    component: FormsComponent
  },
  {
    path: 'grid',
    component: GridComponent
  },
  {
    path: 'icons',
    component: IconsComponent
  },
  {
    path: 'tables',
    component: TablesComponent
  },
  {
    path: 'typography',
    component: TypographyComponent
  },
  {
    path: 'accordion',
    component: AccordionComponent
  },
  {
    path: 'alert',
    component: AlertComponent
  },
  {
    path: 'buttons',
    component: ButtonsComponent
  },
  {
    path: 'carousel',
    component: CarouselComponent
  },
  {
    path: 'collapse',
    component: CollapseComponent
  },
  {
    path: 'datepicker',
    component: DatepickerComponent
  },
  {
    path: 'dropdown',
    component: DropdownComponent
  },
  {
    path: 'modal',
    component: ModalComponent
  },
  {
    path: 'pagination',
    component: PaginationComponent
  },
  {
    path: 'popover',
    component: PopoverComponent
  },
  {
    path: 'progressbar',
    component: ProgressbarComponent
  },
  {
    path: 'rating',
    component: RatingComponent
  },
  {
    path: 'select',
    component: SelectComponent
  },
  {
    path: 'tabset',
    component: TabsetComponent
  },
  {
    path: 'timepicker',
    component: TimepickerComponent
  },
  {
    path: 'tooltip',
    component: TooltipComponent
  },
  {
    path: 'typeahead',
    component: TypeaheadComponent
  },
  {
    path: 'lease-details',
    component: LeaseDetailsComponent
  },
  {
    path: 'border',
    component: BorderComponent
  },
  {
    path: 'display',
    component: DisplayComponent
  },
  {
    path: 'flexbox',
    component: FlexboxComponent
  },
  {
    path: 'float',
    component: FloatComponent
  },
  {
    path: 'other',
    component: OtherComponent
  },
  {
    path: 'position',
    component: PositionComponent
  },
  {
    path: 'sizing',
    component: SizingComponent
  },
  {
    path: 'spacing',
    component: SpacingComponent
  },
  {
    path: 'text',
    component: TextComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
